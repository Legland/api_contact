﻿using Contacts_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Contacts_API.ViewModel
{
    public class SkillAndContactViewModel
    {
        public List<ContactModel> Contacts { get; set; }
        public List<SkillModel> Skills { get; set; }
        public ContactSkillModel ContactSkill { get; set; }
        public List<ContactSkillModel> ContactSkills { get; set; }
    }
}

