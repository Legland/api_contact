﻿using Contacts_API.Data;
using Contacts_API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
#pragma warning disable 1587
////////////////////////////////////////
/// Skill Cotroller                  //
/// Create by Legland Ludovic        //
/// Creation date: 06.07.2020        //
///////////////////////////////////////
#pragma warning restore 1587
namespace Contacts_API.Areas.User.Controllers
{
    [Area("User")]
    public class SkillController : Controller
    {
        private readonly ApplicationDbContext _db;
        /// <summary>
        /// set the dbContext
        /// </summary>
        /// <param name="db"></param>
        public SkillController(ApplicationDbContext db)
        {
            _db = db;
        }
        /// <summary>
        /// Initialse the view index with the table skill
        /// </summary>
        /// <returns>View with Skill table</returns>
        [HttpGet("Skill")]
        public async Task<IActionResult> Index()
        {
            return View(await _db.DbSkill.ToListAsync());
        }

        /// <summary>
        /// Add or edit the field
        /// add if the id is 0 else return the view with the id who will be update
        /// </summary>
        /// <param name="id"></param>
        /// <returns>the Create's view if it's new elese return the view with id</returns>

        // GET: User/Skill/Create
        [HttpGet("Skill/AddOrEdit")]
        public IActionResult AddOrEdit(int? id = 0)
        {
            if (id == null)
            {
                return NotFound();
            }
            else if (id == 0)
            {
                return View(new SkillModel());
            }
            else
            {
                return View(_db.DbSkill.Find(id));
            }
        }

        /// <summary>
        /// Ïnsert the skill if Id = 0 else update it
        /// </summary>
        /// <param name="id">id to update</param>
        /// <param name="skill"> the skill who will be update to the database </param>
        /// <returns></returns>
        [HttpPost("Skill/AddOrEdit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddOrEdit(int id, [Bind("Id_skill,Ski_name,Ski_level")] SkillModel skill)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (skill.Id_skill == 0)
                    {
                        _db.Add(skill);
                    }
                    else
                    {
                        _db.Update(skill);
                    }
                    await _db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SkillModelExists(skill.Id_skill))
                    {
                        return NotFound();
                    }
                    else 
                    {
                       
                    }
                }
                catch(Exception)
                {

                }
                return RedirectToAction(nameof(Index));
            }
            return View(skill);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">id of the skill who will be delete</param>
        /// <returns>redirect to the index View </returns>

        [HttpGet("Skill/Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var skill = await _db.DbSkill
                .FirstOrDefaultAsync(m => m.Id_skill == id);
            if (skill == null)
            {
                return NotFound();
            }
            _db.DbSkill.Remove(skill);
            await _db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        private bool SkillModelExists(int id)
        {
            return _db.DbSkill.Any(e => e.Id_skill == id);
        }
    }
}
