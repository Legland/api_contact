﻿using Contacts_API.Data;
using Contacts_API.Models;
using Contacts_API.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

#pragma warning disable 1587
///////////////////////////////////////////////
/// HomeController                           //
/// This controler show the contact's list   //
/// and every skill for the selected contact //
///                                          //           
/// Create by Legland Ludovic                //
/// Creation date: 07.07.2020                //
///////////////////////////////////////////////
#pragma warning restore 1587

namespace Contacts_API.Controllers
{
    [Area(areaName: "user")]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _db;
        /// <summary>
        /// set the dbContext and the log
        /// </summary>
        /// <param name="db"></param>
        /// <param name="logger"></param>
        public HomeController(ILogger<HomeController> logger, ApplicationDbContext db)
        {
            _logger = logger;
            _db = db;
        }
        /// <summary>
        /// initialize the View withe the ViewModel SkillAndContactViewModel with the db context of the 3 tables Contact, skill and contactSkill
        /// the skills list is empty at the beginning
        /// </summary>
        /// <returns>iew with model</returns> 
        public async Task<IActionResult> Index()
        {

            SkillAndContactViewModel my_model = new SkillAndContactViewModel()
            {
                Contacts = await _db.DbContact.OrderBy(c => c.Con_full_name).ToListAsync(),
                Skills = new List<SkillModel>(),
                ContactSkills = await _db.DbContactSkill.ToListAsync(),
                ContactSkill = new ContactSkillModel()
            };
            return View(my_model);
        }
        /// <summary>
        ///  Retrieve the contact's skills
        /// </summary>
        /// <param name="id"> contact's id</param>
        /// <returns>return to the index view with the new model</returns>
        [HttpGet("ShowSkill")]
        public async Task<IActionResult> ShowSkill(int? id)
        {
            SkillAndContactViewModel my_modelUpdated = new SkillAndContactViewModel()
            {
                Contacts = await _db.DbContact.OrderBy(c => c.Con_full_name).ToListAsync(),
                Skills = await (
                from s in _db.DbSkill
                join cs in _db.DbContactSkill on s.Id_skill equals cs.Cs_skillId
                join c in _db.DbContact on cs.Cs_contactId equals c.Id_contact
                where c.Id_contact == id
                select s).ToListAsync(),
                ContactSkills = await _db.DbContactSkill.ToListAsync(),
                ContactSkill = new ContactSkillModel()
            };

            return View("index", my_modelUpdated);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
