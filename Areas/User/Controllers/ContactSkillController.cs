﻿using Contacts_API.Data;
using Contacts_API.Models;
using Contacts_API.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
#pragma warning disable 1587
//////////////////////////////////////////
/// ContactSkillController              //
/// This controler  manage the link     //    
/// between the table Contact and skill //
///                                     //           
/// Create by Legland Ludovic           //
/// Creation date: 06.07.2020           //
//////////////////////////////////////////
#pragma warning restore 1587
namespace Contacts_API.Areas.User.Controllers
{
    [Area("User")]
    public class ContactSkillController : Controller
    {
        private readonly ApplicationDbContext _db;

        /// <summary>
        /// Set the db context
        /// </summary>
        /// <param name="db"></param>
        public ContactSkillController(ApplicationDbContext db)
        {
            _db = db;
        }

        /// <summary>
        /// initialize the View withe the ViewModel SkillAndContactViewModel with the db context of the 3 tables Contact, skill and contactSkill
        /// Initialize each skills and contacts who are link together
        /// </summary>
        /// <returns>View(model)</returns>
        [HttpGet("link")]
        public async Task<IActionResult> Index()
        {
            SkillAndContactViewModel my_model = new SkillAndContactViewModel()
            {
                Contacts = await _db.DbContact.ToListAsync(),
                Skills = await _db.DbSkill.OrderBy(p => p.Ski_name).Distinct().ToListAsync(),
                ContactSkills = await _db.DbContactSkill.ToListAsync(),
                ContactSkill = new ContactSkillModel()
            };

            foreach (ContactSkillModel conSkill in my_model.ContactSkills)
            {
                conSkill.Skill = null;
                conSkill.Contact = null;

                conSkill.Skill = (from s in _db.DbSkill
                                  where s.Id_skill == conSkill.Cs_skillId
                                  orderby s.Ski_name,s.Ski_level
                                  select s).First();
                conSkill.Contact = (from c in _db.DbContact
                                    where c.Id_contact == conSkill.Cs_contactId
                                    orderby c.Con_full_name,c.Con_adress
                                    select c).First();
            }
            return View(my_model);
        }
        /// <summary>
        /// Retrieve every id_skill who are selected in the view and insert the in the table contact skill to create the link 
        /// </summary>
        /// <param name="model"></param>
        /// <returns>redirect to the index View </returns>
        [HttpPost("link/link")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Link(SkillAndContactViewModel model)
        {
            ContactSkillModel skillModel = new ContactSkillModel();

            string selected = Request.Form["chkidSelected"].ToString();
            string[] selectedListId = selected.Split(',');

            foreach (var temp in selectedListId)
            {
                if (selectedListId.Count() > 0 && !string.IsNullOrEmpty(temp))
                {
                    try
                    {
                        skillModel.Cs_contactId = model.ContactSkill.Cs_contactId;
                        skillModel.Cs_skillId = int.Parse(temp);
                        _db.Add(skillModel);
                        await _db.SaveChangesAsync();
                    }
                    catch(Exception)
                    {

                    }

                }
            }
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Retrive both id skill and contact who are merge in the view 
        /// and split them in order to delete the right line
        /// </summary>
        /// <param name="id"></param>
        /// <returns>redirect to the index View </returns>

        [HttpGet("link/delete")]
        public async Task<IActionResult> Delete(string id)
        {
            string[] selectedListId = id.Split('$');
            int? idContact = int.Parse(selectedListId[0].ToString());
            int? idSkill = int.Parse(selectedListId[1].ToString());

            if (idContact == null || idSkill == null || idContact<1 || idSkill <1)
            {
                return NotFound();
            }
            var contactSkill = await _db.DbContactSkill
                //.Include(c => c.Contact)
                .FirstOrDefaultAsync(m => m.Cs_contactId == idContact && m.Cs_skillId == idSkill);
            if (contactSkill == null)
            {
                return NotFound();
            }

            _db.DbContactSkill.Remove(contactSkill);
            await _db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ContactSkillModelExists(int id)
        {
            return _db.DbContactSkill.Any(e => e.Cs_contactId == id);
        }
    }
}
