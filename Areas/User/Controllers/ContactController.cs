﻿using Contacts_API.Data;
using Contacts_API.Extention;
using Contacts_API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;


#pragma warning disable 1587
///////////////////////////////////////////
/// Contact Controller                   //
/// This controler  manage the contact  //
/// with CRUD Methode                   //            
///                                     //           
/// Create by Legland Ludovic           //
/// Creation date: 06.07.2020           //
//////////////////////////////////////////
#pragma warning restore 1587

namespace Contacts_API.Areas.User.Controllers
{
    [Area(areaName: "user")]
    public class ContactController : Controller
    {
        private readonly ApplicationDbContext _dbContact;
        /// <summary>
        /// set the dbContext
        /// </summary>
        /// <param name="db"></param>
        public ContactController(ApplicationDbContext db)
        {
            _dbContact = db;
        }
        /// <summary>
        /// Initialse the view index with the table contact
        /// </summary>
        /// <returns>View with Contact table</returns>
        [HttpGet("Contact")]
        public async Task<IActionResult> Index()
        { /*
            var userId = User.GetLoggedInUserId<string>();
            if (userId == null)
            {
                return RedirectToAction("/Identity/Account/Register");
            }*/
            return View(await _dbContact.DbContact.ToListAsync());
        }

        /// <summary>
        /// Add or edit the field
        /// add if the id is 0 else return the view with the id who will be update
        /// </summary>
        /// <param name="id"></param>
        /// <returns>the Create's view if it's new elese return the view with id</returns>
        [HttpGet("Contact/AddOrEdit")]
        public IActionResult AddOrEdit(int? id = 0)
        {
            if (id == null)
            {
                return NotFound();
            }
            else if (id == 0)
            {
                return View(new ContactModel());
            }
            else
            {
                return View(_dbContact.DbContact.Find(id));
            }
        }

        /// <summary>
        ///  Ïnsert the contact if Id = 0 else update it
        /// </summary>
        /// <param name="contact"> the contact Model update to the database</param>
        /// <returns> redirect to the index View </returns>
        [HttpPost("Contact/AddOrEdit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddOrEdit([Bind("Id_contact,Con_last_name,Con_first_name,Con_full_name,Con_adress,Con_email,Con_phone")] ContactModel contact)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userId = User.GetLoggedInUserId<string>();
                    if (contact.Id_contact == 0)
                    {
                        if (userId != null)
                        {
                            contact.Con_idUser = userId.ToString();
                        }
                        _dbContact.Add(contact);
                    }
                    else
                    {
                        _dbContact.Update(contact);
                    }
                    await _dbContact.SaveChangesAsync();
                }
                catch (Exception)
                {

                }
                return RedirectToAction(nameof(Index));
            }
            return View(contact);
        }

        /// <summary>
        /// Delete the contact id and update it to the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns> redirect to the same page</returns>

        [HttpGet("Contact/Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contact = await _dbContact.DbContact
                .FirstOrDefaultAsync(m => m.Id_contact == id);
            if (contact == null)
            {
                return NotFound();
            }
            _dbContact.DbContact.Remove(contact);
            await _dbContact.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        private bool ContactModelExists(int id)
        {
            return _dbContact.DbContact.Any(e => e.Id_contact == id);
        }
    }
}
