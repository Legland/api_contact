USE [Contacts_API]
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount]) VALUES (N'4294d25d-f640-4207-9483-d459fcd895bb', N'ludovic.legland@hotmail.com', N'LUDOVIC.LEGLAND@HOTMAIL.COM', N'ludovic.legland@hotmail.com', N'LUDOVIC.LEGLAND@HOTMAIL.COM', 1, N'AQAAAAEAACcQAAAAENR5n4d6IO1Bt3WUVRq8C/w6fej3OiP2frT0+ascy6Iy8/FY36EG5bwmHE2UoIWUvA==', N'QANH35ZLMVGZSVAD6LTDDCJ5OBGWRZTN', N'33c0783b-8bbc-40fe-8fce-95bc91d46db5', N'0792064962', 0, 0, NULL, 1, 0)
GO
SET IDENTITY_INSERT [dbo].[DbContact] ON 

INSERT [dbo].[DbContact] ([Id_contact], [Con_last_name], [Con_first_name], [Con_full_name], [Con_adress], [Con_email], [Con_phone], [Con_idUser]) VALUES (10, N'Legland', N'Ludovic', N'Annie Peclet', N'Av. Artichaux 21
1004 Lausanne', N'ludovic.legland@hotmail.com', N'0792064962', NULL)
INSERT [dbo].[DbContact] ([Id_contact], [Con_last_name], [Con_first_name], [Con_full_name], [Con_adress], [Con_email], [Con_phone], [Con_idUser]) VALUES (11, N'Peclet', N'Annie', N'Annie Peclet', N'Ch. des Accacias 31
1002 Pastilles', N'peclet.annie@gmail.com', N'0782342321', N'4294d25d-f640-4207-9483-d459fcd895bb')
SET IDENTITY_INSERT [dbo].[DbContact] OFF
GO
SET IDENTITY_INSERT [dbo].[DbSkill] ON 

INSERT [dbo].[DbSkill] ([Id_skill], [Ski_name], [Ski_level]) VALUES (8, N'Anglais', N'Demonstrating')
INSERT [dbo].[DbSkill] ([Id_skill], [Ski_name], [Ski_level]) VALUES (10, N'C#', N'Basic')
INSERT [dbo].[DbSkill] ([Id_skill], [Ski_name], [Ski_level]) VALUES (6, N'C++', N'Expert')
INSERT [dbo].[DbSkill] ([Id_skill], [Ski_name], [Ski_level]) VALUES (2, N'Perl', N'Low')
INSERT [dbo].[DbSkill] ([Id_skill], [Ski_name], [Ski_level]) VALUES (13, N'PHP', N'Low')
SET IDENTITY_INSERT [dbo].[DbSkill] OFF
GO
INSERT [dbo].[DbContactSkill] ([Cs_Contactid], [cs_SkillId], [SkillId_skill]) VALUES (10, 10, NULL)
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'00000000000000_CreateIdentitySchema', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200706084917_Add_Contact_and_skill', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200706122315_AlterTableModifySkill_lvl', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200706170918_modifylink', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200706172834_correct link', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200706193538_ChangeContactSkillName', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200707122849_add_ownerid', N'3.1.5')
GO
