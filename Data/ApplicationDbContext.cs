﻿using Contacts_API.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Contacts_API.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<ContactModel> DbContact { get; set; }
        public DbSet<SkillModel> DbSkill { get; set; }
        public DbSet<ContactSkillModel> DbContactSkill { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ContactSkillModel>().HasKey(key => new { key.Cs_contactId, key.Cs_skillId });

            modelBuilder.Entity<ContactSkillModel>().HasOne(tble => tble.Contact)
                .WithMany(tble => tble.Sc_contact_Skill).HasForeignKey(key => key.Cs_contactId);

            modelBuilder.Entity<ContactSkillModel>().HasOne(tble => tble.Contact)
          .WithMany(tble => tble.Sc_contact_Skill).HasForeignKey(key => key.Cs_skillId);

            modelBuilder.Entity<SkillModel>()
            .HasIndex(p => new { p.Ski_name, p.Ski_level }).IsUnique();
        }

        
    }

  
}
