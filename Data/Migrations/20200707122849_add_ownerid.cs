﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Contacts_API.Data.Migrations
{
    public partial class add_ownerid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Con_idUser",
                table: "DbContact",
                type: "nvarchar(300)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Con_idUser",
                table: "DbContact");
        }
    }
}
