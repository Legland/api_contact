﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Contacts_API.Data.Migrations
{
    public partial class Add_Contact_and_skill : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DbContact",
                columns: table => new
                {
                    Id_contact = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Con_last_name = table.Column<string>(type: "nvarchar(250)", nullable: false),
                    Con_first_name = table.Column<string>(type: "nvarchar(250)", nullable: false),
                    Con_full_name = table.Column<string>(type: "nvarchar(400)", nullable: true),
                    Con_adress = table.Column<string>(type: "nvarchar(800)", nullable: false),
                    Con_email = table.Column<string>(type: "nvarchar(300)", nullable: false),
                    Con_phone = table.Column<string>(type: "nvarchar(300)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbContact", x => x.Id_contact);
                });

            migrationBuilder.CreateTable(
                name: "DbSkill",
                columns: table => new
                {
                    Id_skill = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ski_name = table.Column<string>(type: "nvarchar(250)", nullable: false),
                    Ski_level = table.Column<string>(type: "nvarchar(250)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbSkill", x => x.Id_skill);
                });

            migrationBuilder.CreateTable(
                name: "ContactSkillModel",
                columns: table => new
                {
                    Cs_contactid = table.Column<int>(nullable: false),
                    Cs_skill_id = table.Column<int>(nullable: false),
                    SkillId_skill = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactSkillModel", x => new { x.Cs_contactid, x.Cs_skill_id });
                    table.ForeignKey(
                        name: "FK_ContactSkillModel_DbContact_Cs_skill_id",
                        column: x => x.Cs_skill_id,
                        principalTable: "DbContact",
                        principalColumn: "Id_contact",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ContactSkillModel_DbSkill_SkillId_skill",
                        column: x => x.SkillId_skill,
                        principalTable: "DbSkill",
                        principalColumn: "Id_skill",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ContactSkillModel_Cs_skill_id",
                table: "ContactSkillModel",
                column: "Cs_skill_id");

            migrationBuilder.CreateIndex(
                name: "IX_ContactSkillModel_SkillId_skill",
                table: "ContactSkillModel",
                column: "SkillId_skill");

            migrationBuilder.CreateIndex(
                name: "IX_DbSkill_Ski_name_Ski_level",
                table: "DbSkill",
                columns: new[] { "Ski_name", "Ski_level" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ContactSkillModel");

            migrationBuilder.DropTable(
                name: "DbContact");

            migrationBuilder.DropTable(
                name: "DbSkill");
        }
    }
}
