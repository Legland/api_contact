﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Contacts_API.Data.Migrations
{
    public partial class ChangeContactSkillName : Migration
    {
       protected override void Up(MigrationBuilder migrationBuilder)
        {
            /*   migrationBuilder.DropForeignKey(
                  name: "FK_ContactSkillModel_DbContact_Cs_skillId",
                  table: "ContactSkillModel");

              migrationBuilder.DropForeignKey(
                  name: "FK_ContactSkillModel_DbSkill_SkillId_skill",
                  table: "ContactSkillModel");

              migrationBuilder.DropPrimaryKey(
                  name: "PK_ContactSkillModel",
                  table: "ContactSkillModel");

              migrationBuilder.RenameTable(
                  name: "ContactSkillModel",
                  newName: "DbContactSkill");

              migrationBuilder.RenameIndex(
                  name: "IX_ContactSkillModel_SkillId_skill",
                  table: "DbContactSkill",
                  newName: "IX_DbContactSkill_SkillId_skill");

              migrationBuilder.RenameIndex(
                  name: "IX_ContactSkillModel_Cs_skillId",
                  table: "DbContactSkill",
                  newName: "IX_DbContactSkill_Cs_skillId");

              migrationBuilder.AddPrimaryKey(
                  name: "PK_DbContactSkill",
                  table: "DbContactSkill",
                  columns: new[] { "Cs_contactId", "Cs_skillId" });

              migrationBuilder.AddForeignKey(
                  name: "FK_DbContactSkill_DbContact_Cs_skillId",
                  table: "DbContactSkill",
                  column: "Cs_skillId",
                  principalTable: "DbContact",
                  principalColumn: "Id_contact",
                  onDelete: ReferentialAction.Cascade);

              migrationBuilder.AddForeignKey(
                  name: "FK_DbContactSkill_DbSkill_SkillId_skill",
                  table: "DbContactSkill",
                  column: "SkillId_skill",
                  principalTable: "DbSkill",
                  principalColumn: "Id_skill",
                  onDelete: ReferentialAction.Restrict);*/
          }

          protected override void Down(MigrationBuilder migrationBuilder)
          {
           /*   migrationBuilder.DropForeignKey(
                  name: "FK_DbContactSkill_DbContact_Cs_skillId",
                  table: "DbContactSkill");

              migrationBuilder.DropForeignKey(
                  name: "FK_DbContactSkill_DbSkill_SkillId_skill",
                  table: "DbContactSkill");

              migrationBuilder.DropPrimaryKey(
                  name: "PK_DbContactSkill",
                  table: "DbContactSkill");

              migrationBuilder.RenameTable(
                  name: "DbContactSkill",
                  newName: "ContactSkillModel");

              migrationBuilder.RenameIndex(
                  name: "IX_DbContactSkill_SkillId_skill",
                  table: "ContactSkillModel",
                  newName: "IX_ContactSkillModel_SkillId_skill");

              migrationBuilder.RenameIndex(
                  name: "IX_DbContactSkill_Cs_skillId",
                  table: "ContactSkillModel",
                  newName: "IX_ContactSkillModel_Cs_skillId");

              migrationBuilder.AddPrimaryKey(
                  name: "PK_ContactSkillModel",
                  table: "ContactSkillModel",
                  columns: new[] { "Cs_contactId", "Cs_skillId" });

              migrationBuilder.AddForeignKey(
                  name: "FK_ContactSkillModel_DbContact_Cs_skillId",
                  table: "ContactSkillModel",
                  column: "Cs_skillId",
                  principalTable: "DbContact",
                  principalColumn: "Id_contact",
                  onDelete: ReferentialAction.Cascade);

              migrationBuilder.AddForeignKey(
                  name: "FK_ContactSkillModel_DbSkill_SkillId_skill",
                  table: "ContactSkillModel",
                  column: "SkillId_skill",
                  principalTable: "DbSkill",
                  principalColumn: "Id_skill",
                  onDelete: ReferentialAction.Restrict);*/
          }
      }
}
