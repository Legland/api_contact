﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Contacts_API.Data.Migrations
{
    public partial class correctlink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           /* migrationBuilder.DropForeignKey(
                name: "FK_ContactSkillModel_DbContact_Cs_skill_id",
                table: "ContactSkillModel");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ContactSkillModel",
                table: "ContactSkillModel");

            migrationBuilder.DropIndex(
                name: "IX_ContactSkillModel_Cs_skill_id",
                table: "ContactSkillModel");

            migrationBuilder.DropColumn(
                name: "Cs_skill_id",
                table: "ContactSkillModel");

            migrationBuilder.RenameColumn(
                name: "Cs_contactid",
                table: "ContactSkillModel",
                newName: "Cs_contactId");

            migrationBuilder.AddColumn<int>(
                name: "Cs_skillId",
                table: "ContactSkillModel",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ContactSkillModel",
                table: "ContactSkillModel",
                columns: new[] { "Cs_contactId", "Cs_skillId" });

            migrationBuilder.CreateIndex(
                name: "IX_ContactSkillModel_Cs_skillId",
                table: "ContactSkillModel",
                column: "Cs_skillId");

            migrationBuilder.AddForeignKey(
                name: "FK_ContactSkillModel_DbContact_Cs_skillId",
                table: "ContactSkillModel",
                column: "Cs_skillId",
                principalTable: "DbContact",
                principalColumn: "Id_contact",
                onDelete: ReferentialAction.Cascade);*/
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
          /*  migrationBuilder.DropForeignKey(
                name: "FK_ContactSkillModel_DbContact_Cs_skillId",
                table: "ContactSkillModel");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ContactSkillModel",
                table: "ContactSkillModel");

            migrationBuilder.DropIndex(
                name: "IX_ContactSkillModel_Cs_skillId",
                table: "ContactSkillModel");

            migrationBuilder.DropColumn(
                name: "Cs_skillId",
                table: "ContactSkillModel");

            migrationBuilder.RenameColumn(
                name: "Cs_contactId",
                table: "ContactSkillModel",
                newName: "Cs_contactid");

            migrationBuilder.AddColumn<int>(
                name: "Cs_skill_id",
                table: "ContactSkillModel",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ContactSkillModel",
                table: "ContactSkillModel",
                columns: new[] { "Cs_contactid", "Cs_skill_id" });

            migrationBuilder.CreateIndex(
                name: "IX_ContactSkillModel_Cs_skill_id",
                table: "ContactSkillModel",
                column: "Cs_skill_id");

            migrationBuilder.AddForeignKey(
                name: "FK_ContactSkillModel_DbContact_Cs_skill_id",
                table: "ContactSkillModel",
                column: "Cs_skill_id",
                principalTable: "DbContact",
                principalColumn: "Id_contact",
                onDelete: ReferentialAction.Cascade);*/
        }
    }
}
