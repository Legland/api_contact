﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#pragma warning disable 1587
////////////////////////////////////////
/// Contact model                     //
/// Create by Legland Ludovic        //
/// Creation date: 06.07.2020        //
///////////////////////////////////////
#pragma warning restore 1587
namespace Contacts_API.Models
{
    public class ContactModel
    {
        /// <summary>
        ///  Contact's Primary key
        /// </summary>
        [Key]
        [Column(TypeName = "int")]
        public int Id_contact { get; set; }

        /// <summary>
        ///  Contact's Last name 
        /// </summary>
        [Column(TypeName = "nvarchar(250)")]
        [Required(ErrorMessage = "This field is required")]
        [DisplayName("Last Name")]
        public string Con_last_name { get; set; }

        /// <summary>
        /// Contact's first name
        /// </summary>
        [Column(TypeName = "nvarchar(250)")]
        [Required(ErrorMessage = "This field is required")]
        [DisplayName("First Name")]
        public string Con_first_name { get; set; }

        /// <summary>
        ///  Contact's full name
        /// </summary>

        [Column(TypeName = "nvarchar(400)")]
        [DisplayName("Full Name")]
        [Required]
        public string Con_full_name { get; set; }
        

        /// <summary>
        /// contact's adress
        /// </summary>

        [Column(TypeName = "nvarchar(800)")]
        [Required(ErrorMessage = "This field is required")]
        [DisplayName("Address")]
        [DataType(DataType.MultilineText)]
        public string Con_adress { get; set; }
        
        /// <summary>
        /// Contact's email
        /// </summary>

        [Column(TypeName = "nvarchar(300)")]
        [Required(ErrorMessage = "This field is required")]
        // [RegularExpression(@"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$", ErrorMessage = "Please enter a valid email")]
        [EmailAddress]
        [DisplayName("Email")]
        public string Con_email { get; set; }

        /// <summary>
        /// Contact's phone number could be mobile or fix
        /// </summary>
        [Column(TypeName = "nvarchar(300)")]
        [Required(ErrorMessage = "This field is required")]
        [Phone]
        [DisplayName("Mobile Phone")]
        public string Con_phone { get; set; }

        /// <summary>
        /// Contact's list of skill
        /// </summary>
          public List<ContactSkillModel> Sc_contact_Skill { get; set; }

        [Column(TypeName = "nvarchar(300)")]
        public string Con_idUser { get; set; }
    }
}

