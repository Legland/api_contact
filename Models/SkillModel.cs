﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


#pragma warning disable 1587
/////////////////////////////////////////
/// Skill model                        //
/// Create by Legland Ludovic          //
/// Creation date: 06.07.2020          //
////////////////////////////////////////
#pragma warning restore 1587

namespace Contacts_API.Models
{
    public class SkillModel
    {
        /// <summary>
        /// Skill's primary key
        /// </summary>

        [Key]
        [Column(TypeName = "int")]
        public int Id_skill { get; set; }

        /// <summary>
        ///  skill's Name
        /// </summary>
     
        [Column(TypeName = "nvarchar(250)")]
        [Required(ErrorMessage = "This field is required")]
        [DisplayName("Name")]
        public string Ski_name { get; set; }

        /// <summary>
        ///  the skill's level
        /// </summary>

        [Column(TypeName = "nvarchar(250)")]
        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Level (Expertise)")]
        public string Ski_level { get; set; }
        /// <summary>
        /// the skill's list of Contact
        /// </summary>
       /* [Display (Name = "Skill")]
        public int Ski_ContactSkillModelId { get; set; }
        [ForeignKey("ski_ContactSkillModelId")]*/
        public List<ContactSkillModel> Sc_contact_Skill { get; set; }
    }
}
