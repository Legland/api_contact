﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

#pragma warning disable 1587
////////////////////////////////////////
/// Contact skill model               //
/// Create by Legland Ludovic        //
/// Creation date: 06.07.2020        //
///////////////////////////////////////
#pragma warning restore 1587

namespace Contacts_API.Models
{
    public class ContactSkillModel
    { 
        /// <summary>
        /// Contact's foreigh key
        /// </summary>
        public int Cs_contactId { get; set; }
        /// <summary>
        /// Contact's object
        /// </summary>
        public ContactModel Contact { get; set; }
        /// <summary>
        /// Skill's foreigh key
        /// </summary>
        public int Cs_skillId { get; set; }
        /// <summary>
        /// skill's object
        /// </summary>
        public SkillModel Skill { get; set; }
    }
}
