﻿using System;
using System.Security.Claims;

#pragma warning disable 1587
////////////////////////////////////////
/// CurrentUserExtention             //
/// this extention return            //   
/// the current user's id            //   
///                                  //
/// Create by Legland Ludovic        //
/// Creation date: 07.07.2020        //
///////////////////////////////////////
#pragma warning restore 1587

namespace Contacts_API.Extention
{
    public static class CurrentUserExtention
    {

        public static T GetLoggedInUserId<T>(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));

            var loggedInUserId = principal.FindFirstValue(ClaimTypes.NameIdentifier);

            if (typeof(T) == typeof(string))
            {
                return (T)Convert.ChangeType(loggedInUserId, typeof(T));
            }
            else if (typeof(T) == typeof(int) || typeof(T) == typeof(long))
            {
                return loggedInUserId != null ? (T)Convert.ChangeType(loggedInUserId, typeof(T)) : (T)Convert.ChangeType(0, typeof(T));
            }
            else
            {
                throw new Exception("Invalid type provided");
            }
        }
    }
}
