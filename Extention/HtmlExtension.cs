﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

#pragma warning disable 1587
//////////////////////////////////////////////
/// HtmlExtensions                          //
/// this extention disable or enable field  //   
///                                         //
/// Create by Legland Ludovic               //
/// Creation date: 07.07.2020               //
//////////////////////////////////////////////
#pragma warning restore 1587

namespace Contacts_API.Extention
{
    public static class HtmlExtensions
    {
        public static IHtmlContent DisabledIf(this IHtmlHelper htmlHelper,
                                              bool condition)
        => new HtmlString(condition ? "disabled=\"disabled\"" : "");
    }
}
