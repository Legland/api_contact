﻿#pragma warning disable 1587
//////////////////////////////////////////////
/// ReflexionsExtention                     //
/// return the value of the item passed     //
/// in the function                         //
///                                         //
/// Create by Legland Ludovic               //
/// Creation date: 07.07.2020               //
//////////////////////////////////////////////
#pragma warning restore 1587

namespace Contacts_API.Areas.Extention
{
    public static class ReflexionsExtention
    {
        public static string GetPropertyValue<T>(this T item, string propertyName)
        {
            return item.GetType().GetProperty(propertyName).GetValue(item, null).ToString();
        }
    }
}
