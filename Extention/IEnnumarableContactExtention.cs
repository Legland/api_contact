﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

#pragma warning disable 1587
//////////////////////////////////////////////
/// IEnnumarableContactExtention            //
/// this extention return a list of         //
/// contact's item                          //    
///                                         //
/// Create by Legland Ludovic               //
/// Creation date: 07.07.2020               //
//////////////////////////////////////////////
#pragma warning restore 1587

namespace Contacts_API.Areas.Extention
{
    public static class IEnnumarableContactExtention
    {
        public static IEnumerable<SelectListItem> ToSelectListItem<T>(this IEnumerable<T> items, int selectedValue)
        {
            return from item in items
                   select new SelectListItem
                   {
                       Text = item.GetPropertyValue("Con_full_name"),
                       Value = item.GetPropertyValue("Id_contact"),
                       Selected = item.GetPropertyValue("Id_contact").Equals(selectedValue.ToString())
                   };
        }
    }
}
